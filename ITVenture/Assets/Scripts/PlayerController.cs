﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	private Animator anim;
	private bool playerMoving = false;
	private Vector2 lastmove;
	private Rigidbody2D playerrigidbody;
	private static bool playerExists;
	private bool attacking;
	public float attackTime;
	private float attackTimeC;
	public GameObject weapon;
	public string startPoint;

	public float diagonalMoveModifier;
	private float currentMoveSpeed;
	public bool canMove;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();		
		playerrigidbody = GetComponent<Rigidbody2D> ();
		if (!playerExists) {
			playerExists = true;
			DontDestroyOnLoad (transform.gameObject);
		} else {
			
			Destroy (gameObject);
		}
		canMove = true;
	}
	
	// Update is called once per frame
	void Update () {
		playerMoving = false;
		if (!canMove) {
			playerrigidbody.velocity = Vector2.zero;
			return;
		}
		if (!attacking) {
			if (CnInputManager.GetAxisRaw ("Horizontal") > 0.5f || CnInputManager.GetAxisRaw ("Horizontal") < -0.5f) {
				playerMoving = true;
				//transform.Translate (new Vector3 (Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime,0f,0f));
				playerrigidbody.velocity = new Vector2 (CnInputManager.GetAxisRaw ("Horizontal") * currentMoveSpeed, playerrigidbody.velocity.y);
				lastmove = new Vector2 (CnInputManager.GetAxisRaw ("Horizontal"), 0);
			}

			if (CnInputManager.GetAxisRaw ("Vertical") > 0.5f || CnInputManager.GetAxisRaw ("Vertical") < -0.5f) {
				playerMoving = true;
				//transform.Translate (new Vector3 (0f,Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime,0f));
				playerrigidbody.velocity = new Vector2 (playerrigidbody.velocity.x, CnInputManager.GetAxisRaw ("Vertical") * currentMoveSpeed);
				lastmove = new Vector2 (0, CnInputManager.GetAxisRaw ("Vertical"));
			}

			if (CnInputManager.GetAxisRaw ("Horizontal") < 0.5f && CnInputManager.GetAxisRaw ("Horizontal") > -0.5f) {
				playerrigidbody.velocity = new Vector2 (0f, playerrigidbody.velocity.y);
			}
			if (CnInputManager.GetAxisRaw ("Vertical") < 0.5f && CnInputManager.GetAxisRaw ("Vertical") > -0.5f) {
				playerrigidbody.velocity = new Vector2 (playerrigidbody.velocity.x, 0f);
			}
			if (CnInputManager.GetButtonDown ("Jump")) {
				attackTimeC = attackTime;
				attacking = true;
				//playerrigidbody.velocity = Vector2.zero;
				anim.SetBool ("Attacking", true);
				weapon.SetActive (true);
			}
			if (CnInputManager.GetButtonDown ("Cancel")) {
				Application.Quit ();
			}
			if (Mathf.Abs (CnInputManager.GetAxisRaw ("Horizontal")) > 0.5f && Mathf.Abs (CnInputManager.GetAxisRaw ("Vertical")) > 0.5f) {
				currentMoveSpeed = moveSpeed / 2 * diagonalMoveModifier;
			} else {
				currentMoveSpeed = moveSpeed;
			}
		}
		if (attackTimeC > 0) {
			attackTimeC -= Time.deltaTime;
		}
		if (attackTimeC <= 0) {
			attacking = false;
			anim.SetBool ("Attacking", false);
			weapon.SetActive (false);
		}
		anim.SetFloat ("MoveX", CnInputManager.GetAxisRaw ("Horizontal"));
		anim.SetFloat ("MoveY", CnInputManager.GetAxisRaw ("Vertical"));
		anim.SetBool ("PlayerMoving", playerMoving);
		anim.SetFloat ("LastMoveX", lastmove.x);
		anim.SetFloat ("LastMoveY", lastmove.y);	
	}
}
