﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour {

	public int HealthMax;
	public int HealthCurrent;
	public GameObject healthpickup;
	public int stageId;

	// Use this for initialization
	void Start () {
		SetEnemyMaxHealth ();	
	}

	public void HealEnemy(int healValue){
		HealthCurrent += healValue;
	}
	public void HurtEnemy(int hurtValue){
		HealthCurrent -= hurtValue;
	}

	// Update is called once per frame
	void Update () {
		if (HealthCurrent <= 0) {
			Destroy (gameObject);
			FindObjectOfType<QuestManager> ().KillEnemy (stageId);
			if (Random.Range (0, 12) <= 3)
				Instantiate (healthpickup, transform.position, Quaternion.Euler (Vector3.zero));
		}
	}
	public void SetEnemyMaxHealth(){
		HealthCurrent = HealthMax;
	}
}
