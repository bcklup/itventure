﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnControl : MonoBehaviour {
	
	private MusicController mcontrol;
	private PlayerController thePlayer;
	private CameraController theCamera;

	public string pointName;
	
	// Use this for initialization
	void Start () {
		mcontrol = FindObjectOfType<MusicController> ();
		if (GameObject.FindGameObjectsWithTag ("Player").Length == 0) {
			mcontrol.musicCanPlay = false;
			FindObjectOfType<GControl> ().CreatePlayer ();
		}
		thePlayer = FindObjectOfType <PlayerController>();
		theCamera= FindObjectOfType <CameraController>();

		if (thePlayer.startPoint == pointName) {
			mcontrol.musicCanPlay = false;
			thePlayer.transform.position = transform.position;
			theCamera.transform.position = new Vector3 (transform.position.x, transform.position.y, theCamera.transform.position.z);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
