﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class QuestManager : MonoBehaviour {
	public int[] enemyPerStage;
	private int[] defaultEnemyPerStage;
	private GameObject thePlayer;
	private CameraController theCamera;

	public GameObject[] bosses;
	public bool[] bossSpawned;

	// Use this for initialization
	void Start () {
		thePlayer = GameObject.FindGameObjectWithTag ("Player");
		theCamera = FindObjectOfType<CameraController> ();
		gameObject.SetActive (true);
		defaultEnemyPerStage = enemyPerStage;
	}
	
	// Update is called once per frame
	void Update () {
		if(enemyPerStage[3] <= 0 && !bossSpawned[0])
		{
			Instantiate (bosses [0], GameObject.Find ("BossSpawn").transform.position, Quaternion.Euler(Vector3.zero));
			bossSpawned[0] = true;
			FindObjectOfType<Camera> ().orthographicSize = 5.0f;
		}
		if(enemyPerStage[8] <= 0 && !bossSpawned[1])
		{
			Instantiate (bosses [1], GameObject.Find ("BossSpawn").transform.position, Quaternion.Euler(Vector3.zero));
			bossSpawned[1] = true;
			FindObjectOfType<Camera> ().orthographicSize = 5.0f;
		}
	}
	public void KillEnemy(int stageId){
		switch (stageId){
		case 0:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door2").SetActive (false);
			}
			break;
		case 1:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door3").SetActive (false);
			}
			break;
		case 2:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door4").SetActive (false);
			}
			break;
		case 3:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door5").SetActive (false);
			}
			break;
		case 4:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				FindObjectOfType<Camera> ().orthographicSize = 3.77f;
				FindObjectOfType<NextLevelManager> ().unlockNextLevel ();
				FindObjectOfType<PlayerHealthManager> ().playerHealthMax = 200;
				FindObjectOfType<PlayerHealthManager> ().SetPlayerMaxHealth();
			}
			break;
		case 5:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door7").SetActive (false);
			}
			break;
		case 6:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door8").SetActive (false);
			}
			break;
		case 7:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door9").SetActive (false);
			}
			break;
		case 8:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				GameObject.Find ("door10").SetActive (false);
			}
			break;
		case 9:
			enemyPerStage [stageId] -= 1;
			if (enemyPerStage [stageId] <= 0) {
				FindObjectOfType<Camera> ().orthographicSize = 3.77f;
				FindObjectOfType<PlayerHealthManager> ().playerHealthMax = 300;
				FindObjectOfType<PlayerHealthManager> ().SetPlayerMaxHealth();
			}
			break;
		}
	}
	public void gameOver(){
		thePlayer.GetComponent<PlayerHealthManager> ().SetPlayerMaxHealth ();
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		ResetEnemies ();
	}
	public void ResetEnemies(){
		enemyPerStage = defaultEnemyPerStage;
		bossSpawned [SceneManager.GetActiveScene ().buildIndex - 1] = false;
	}
}
