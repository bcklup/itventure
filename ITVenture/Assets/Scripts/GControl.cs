﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GControl : MonoBehaviour {
	private static bool GCExists;
	public static bool Male = true;
	public GameObject pMale;
	public GameObject pFemale;
	public string PName = "Player";

	public GameObject nameTag;

	// Use this for initialization
	void Start () {
		if (!GCExists) {
			GCExists = true;
			DontDestroyOnLoad (transform.gameObject);
		} else {

			Destroy (gameObject);
		}
		SaveLoad.Load ();
	}
	public void CreatePlayer(){
		if (Male) {
			var PlayerChar = (GameObject)Instantiate (pMale, GameObject.Find ("StartPoint").transform.position, Quaternion.Euler (Vector3.zero));
			//var tag = (GameObject) Instantiate (nameTag, new Vector3(PlayerChar.transform.position.x + 1,PlayerChar.transform.position.y - 0.8f, transform.position.z), Quaternion.Euler(Vector3.zero));
			var tag = (GameObject) Instantiate (nameTag, PlayerChar.transform);
			//tag.transform.parent = PlayerChar.transform;
			tag.GetComponent<NameTagAssign> ().objectName = PName;
		} else {
			var PlayerChar = (GameObject)Instantiate (pFemale, GameObject.Find ("StartPoint").transform.position, Quaternion.Euler (Vector3.zero));
			var tag = (GameObject) Instantiate (nameTag, new Vector3(PlayerChar.transform.position.x,PlayerChar.transform.position.y - 0.8f, transform.position.z), Quaternion.Euler(Vector3.zero));
			tag.transform.parent = PlayerChar.transform;
			tag.GetComponent<NameTagAssign> ().objectName = PName;
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
	public void SetName(string theName){
		PName = theName;
	}
	public void SetGender(int gender){
		if (gender == 0)
			Male = true;
		else if (gender == 1)
			Male = false;
	}
}
