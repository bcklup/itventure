﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeManager : MonoBehaviour {

	public VolumeController[] vcObjects;
	public float currentVolumeLevel;
	public float MaxVolumeLevel = 1.0f;
	private static bool vmExists;


	// Use this for initialization
	void Start () {
		if (!vmExists) {
			vmExists = true;
			DontDestroyOnLoad (transform.gameObject);
		} else {

			Destroy (gameObject);
		}
		vcObjects = FindObjectsOfType<VolumeController> ();
		if (currentVolumeLevel > MaxVolumeLevel) {
			currentVolumeLevel = MaxVolumeLevel;
		}
		foreach (VolumeController vcObject in vcObjects) {
			vcObject.SetAudioLevel (currentVolumeLevel);	
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void SetVolumeLevels(float newVolume){
		if (newVolume > MaxVolumeLevel) {
			currentVolumeLevel = MaxVolumeLevel;
		}else{
			currentVolumeLevel = newVolume;
		}
		foreach (VolumeController vcObject in vcObjects) {
			vcObject.SetAudioLevel (currentVolumeLevel);	
		}
	}
}
