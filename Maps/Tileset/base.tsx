<?xml version="1.0" encoding="UTF-8"?>
<tileset name="kenney-roguelike" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../../YT Tutorial Assets/kenney pack/Spritesheet/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="water" tile="60"/>
  <terrain name="Dirt" tile="640"/>
 </terraintypes>
 <tile id="0" terrain="0,0,0,"/>
 <tile id="1" terrain="0,0,,0"/>
 <tile id="2" terrain=",,,0"/>
 <tile id="3" terrain=",,0,0"/>
 <tile id="4" terrain=",,0,"/>
 <tile id="59" terrain=",0,,0"/>
 <tile id="60" terrain="0,0,0,0"/>
 <tile id="61" terrain="0,,0,"/>
 <tile id="114" terrain="0,,0,0"/>
 <tile id="115" terrain=",0,0,0"/>
 <tile id="116" terrain=",0,,"/>
 <tile id="117" terrain="0,0,,"/>
 <tile id="118" terrain="0,,,"/>
 <tile id="364">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="518" terrain="1,1,1,"/>
 <tile id="519" terrain="1,1,,1"/>
 <tile id="520" terrain=",,,1"/>
 <tile id="521" terrain=",,1,1"/>
 <tile id="522" terrain=",,1,"/>
 <tile id="575" terrain="1,,1,1"/>
 <tile id="576" terrain=",1,1,1"/>
 <tile id="577" terrain=",1,,1"/>
 <tile id="578" terrain="1,1,1,1"/>
 <tile id="579" terrain="1,,1,"/>
 <tile id="583">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="634" terrain=",1,,"/>
 <tile id="635" terrain="1,1,,"/>
 <tile id="636" terrain="1,,,"/>
 <tile id="640">
  <objectgroup draworder="index">
   <object id="9" x="-0.25" y="-0.25" width="16.625" height="16.6875"/>
  </objectgroup>
 </tile>
 <tile id="704">
  <objectgroup draworder="index">
   <object id="3" x="0.909091" y="0.818182" width="15.0909" height="15.2727"/>
  </objectgroup>
 </tile>
 <tile id="705">
  <objectgroup draworder="index">
   <object id="2" x="-0.272727" y="0.545455" width="16.5455" height="15.7273"/>
  </objectgroup>
 </tile>
 <tile id="706">
  <objectgroup draworder="index">
   <object id="5" x="-0.272727" y="0.818182" width="15.3636" height="15.0909"/>
  </objectgroup>
 </tile>
 <tile id="707">
  <objectgroup draworder="index">
   <object id="2" x="0.454545" y="0.545455" width="15.7273" height="15.7273"/>
  </objectgroup>
 </tile>
 <tile id="708">
  <objectgroup draworder="index">
   <object id="3" x="-0.454545" y="0.636364" width="15.8182" height="15.7273"/>
  </objectgroup>
 </tile>
 <tile id="709">
  <objectgroup draworder="index">
   <object id="1" x="0.25" y="-0.25" width="16.125" height="16.5"/>
  </objectgroup>
 </tile>
 <tile id="710">
  <objectgroup draworder="index">
   <object id="2" x="-0.454545" y="-0.272727" width="16.8182" height="16.6364"/>
  </objectgroup>
 </tile>
 <tile id="761">
  <objectgroup draworder="index">
   <object id="6" x="0.727273" y="0.818182" width="14.2727" height="15.2727"/>
  </objectgroup>
 </tile>
 <tile id="762">
  <objectgroup draworder="index">
   <object id="3" x="-0.363636" y="-0.272727" width="16.8182" height="16.5455"/>
  </objectgroup>
 </tile>
 <tile id="763">
  <objectgroup draworder="index">
   <object id="2" x="0.636364" y="-0.363636" width="14.6364" height="16.7273"/>
  </objectgroup>
 </tile>
 <tile id="764">
  <objectgroup draworder="index">
   <object id="2" x="0.363636" y="-0.454545" width="16" height="16.9091"/>
  </objectgroup>
 </tile>
 <tile id="765">
  <objectgroup draworder="index">
   <object id="3" x="-0.454545" y="-0.363636" width="15.9091" height="16.6364"/>
  </objectgroup>
 </tile>
 <tile id="766">
  <objectgroup draworder="index">
   <object id="2" x="-0.454545" y="-0.636364" width="15.9091" height="17.1818"/>
  </objectgroup>
 </tile>
 <tile id="767">
  <objectgroup draworder="index">
   <object id="2" x="-0.454545" y="0.363636" width="16.7273" height="16.1818"/>
  </objectgroup>
 </tile>
 <tile id="818">
  <objectgroup draworder="index">
   <object id="3" x="0.636364" y="-0.363636" width="14.5455" height="16.1705"/>
  </objectgroup>
 </tile>
 <tile id="1055">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="1103">
  <objectgroup draworder="index">
   <object id="1" x="2.36364" y="-0.727273" width="11" height="17"/>
   <object id="3" x="11.8182" y="22.4545"/>
  </objectgroup>
 </tile>
 <tile id="1104">
  <objectgroup draworder="index">
   <object id="1" x="3" y="0" width="10" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1110">
  <objectgroup draworder="index">
   <object id="1" x="2" y="-1" width="12" height="17"/>
  </objectgroup>
 </tile>
 <tile id="1111">
  <objectgroup draworder="index">
   <object id="1" x="2" y="-1" width="12" height="18"/>
  </objectgroup>
 </tile>
 <tile id="1160">
  <objectgroup draworder="index">
   <object id="3" x="0.545455" y="0.545455" width="14.9091" height="15.8182"/>
  </objectgroup>
 </tile>
 <tile id="1161">
  <objectgroup draworder="index">
   <object id="1" x="0.454545" y="-0.727273" width="14.8182" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1167">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1168">
  <objectgroup draworder="index">
   <object id="1" x="0" y="-1" width="16" height="17"/>
  </objectgroup>
 </tile>
 <tile id="1251">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="1252">
  <objectgroup draworder="index"/>
 </tile>
 <tile id="1253">
  <objectgroup draworder="index"/>
 </tile>
</tileset>
